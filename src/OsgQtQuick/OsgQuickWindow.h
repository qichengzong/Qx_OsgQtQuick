/*!
   \author qichengzong@qq.com
   \datetime 2021/01/17 21:42
 */

#pragma once

#include <osg/GraphicsContext>
#include <osgViewer/CompositeViewer>

#include <QQuickWindow>

#include <OsgQtQuickExport.h>

namespace OsgQtQuick 
{
    class OsgQuickView;
    /*! OsgQuickWindow
     */
    class OSGQTQUICK_EXPORT OsgQuickWindow : public QObject
    {
        Q_OBJECT
    public:
        OsgQuickWindow(QQuickWindow *window);

        osg::GraphicsContext* getGraphicsContext() { return m_ptrOsgGraphicsContext.get(); }

        void addView(OsgQuickView* view);

        static OsgQuickWindow* fromWindow(QQuickWindow *window);
    protected slots:
        void frame();

        void sync();
        void cleanup();
    protected:

        void timerEvent(QTimerEvent* event);

        QQuickWindow* m_pWindow;
        int m_iUpdateTimer;

        static std::map<QQuickWindow*, OsgQuickWindow*> s_mapWindows;

        osg::ref_ptr<osg::GraphicsContext> m_ptrOsgGraphicsContext;
        osg::ref_ptr<osgViewer::CompositeViewer> m_ptrCompositeViewer;
    };

}
