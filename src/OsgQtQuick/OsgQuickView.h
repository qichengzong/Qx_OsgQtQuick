/*!
   \author qichengzong@qq.com
   \datetime 2021/01/16 23:00
 */

#pragma once

#include <osgViewer/View>

#include <QQuickItem>
#include <QQuickWindow>

#include <OsgQtQuickExport.h>

namespace OsgQtQuick 
{
    class OsgQuickWindow;
    /*! OsgQuickView Qt Quick Item
     */
    class OSGQTQUICK_EXPORT OsgQuickView : public QQuickItem
    {
        typedef QQuickItem BaseClass;
        Q_OBJECT
    public:
        OsgQuickView(QQuickItem* parent = 0);

        osgViewer::View* getView();
    
        void updateViewport();
    protected slots:
        void handleWindowChanged(QQuickWindow* win);
    
    protected:
        void setKeyboardModifiers(QInputEvent* event);
        QPointF mousePoint(QMouseEvent* event);

        /*! user interface eventss
        */
        void mousePressEvent(QMouseEvent* event);
        void mouseMoveEvent(QMouseEvent* event);
        void mouseReleaseEvent(QMouseEvent* event);
        void mouseDoubleClickEvent(QMouseEvent* event);
        void hoverMoveEvent(QHoverEvent* event);
        void hoverEnterEvent(QHoverEvent* event);
        void wheelEvent(QWheelEvent* event);
        void keyPressEvent(QKeyEvent* event);
        void keyReleaseEvent(QKeyEvent* event);

        void geometryChange(const QRectF& newGeometry, const QRectF& oldGeometry);
        void acceptWindow(OsgQuickWindow *window);

        OsgQuickWindow* m_pWindow = nullptr;   //!< point to window
        osg::ref_ptr<osgViewer::View> m_ptrView;
        osg::ref_ptr<osg::GraphicsContext> m_ptrContext;
    };

}
