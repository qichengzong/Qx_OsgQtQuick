import QtQuick 2.0
import QtQuick.Controls 2.0
import OsgQtQuick 1.0
import QtQuick.Layouts 1.0

import QtQuick.Window 2.2

Rectangle {

    width: 800
    height: 600
    color: "transparent"
    visible: true

    OsgQuickView {
        width: parent.width / 2
        height: parent.height
        id: osgView
        objectName: qsTr("osgView")
    }
    OsgQuickView {
        width: parent.width / 2
        height: parent.height
        x: parent.width / 2
        id: osgView1
        objectName: qsTr("osgView1")
    }
    //    Component.onCompleted: {
    //        console.log(osgView.width, osgView.height)
    //    }
    //    OsgQuickView {
    //        anchors.fill: parent
    //        id: osgView
    //        objectName: qsTr("osgView")
    //    }
} //! [2]
