
#include <iostream>

#include <QGuiApplication>
#include <QQuickView>
#include <QUrl>

#include <osgEarth/Utils>
#include <osgEarth/GLUtils>
#include <osgEarth/MapNode>
#include <osg/MatrixTransform>
#include <osgDB/ReadFile>
#include <osgGA/TrackballManipulator>
#include <osgViewer/ViewerEventHandlers>

#undef osgEarth_FOUND

#ifdef osgEarth_FOUND
#include <osgEarth/EarthManipulator>
#endif

#include <OsgQuickView.h>

int main(int argc, char* argv[])
{
#ifdef osgEarth_FOUND
    osgEarth::initialize();
#endif

    qputenv("QSG_RENDER_LOOP", "basic");

    QGuiApplication app(argc, argv);

    QQuickWindow::setGraphicsApi(QSGRendererInterface::OpenGLRhi);

    qmlRegisterType<OsgQtQuick::OsgQuickView>("OsgQtQuick", 1, 0, "OsgQuickView");
    QQuickView quickView;
	QObject::connect(&quickView, &QQuickView::statusChanged, [&](QQuickView::Status status)
		{
			if (QQuickView::Ready != status) return;

            {
                auto* qView = quickView.contentItem()->findChild<OsgQtQuick::OsgQuickView*>("osgView");
                if (!qView) return;

                auto* view = qView->getView();
                if (!view) return;

                osg::ref_ptr<osg::MatrixTransform> root(new osg::MatrixTransform);
                view->setSceneData(root);
                
                view->addEventHandler(new osgViewer::StatsHandler);
                view->addEventHandler(new osgViewer::HelpHandler);
                osg::ref_ptr<osgGA::CameraManipulator> mp = new osgGA::TrackballManipulator;
#ifdef osgEarth_FOUND
                QString earthFilePath = app.applicationDirPath() + "/../data/earth3D_gaode.earth";
                //mp = new osgEarth::EarthManipulator;
                root->addChild(osgDB::readRefNodeFile(earthFilePath.toLocal8Bit().data()));
#else
                root->addChild(osgDB::readRefNodeFile("cow.osg"));
#endif

                view->setCameraManipulator(mp);
                mp->home(0);
            }

            {
                auto* qView = quickView.contentItem()->findChild<OsgQtQuick::OsgQuickView*>("osgView1");
                if (!qView) return;

                auto* view = qView->getView();
                if (!view) return;

                osg::ref_ptr<osg::MatrixTransform> root(new osg::MatrixTransform);
                view->setSceneData(root);

                view->addEventHandler(new osgViewer::StatsHandler);
                view->addEventHandler(new osgViewer::HelpHandler);

                osg::ref_ptr<osgGA::CameraManipulator> mp = new osgGA::TrackballManipulator;
#ifdef osgEarth_FOUND
                QString earthFilePath = app.applicationDirPath() + "/../data/earth2D_gaode.earth";
               // mp = new osgEarth::EarthManipulator;
                root->addChild(osgDB::readRefNodeFile(earthFilePath.toLocal8Bit().data()));
#else
                root->addChild(osgDB::readRefNodeFile("cow.osg"));
#endif

                view->setCameraManipulator(mp);
                mp->home(0);
            }
		});

	quickView.setResizeMode(QQuickView::SizeRootObjectToView);
	quickView.setSource(QUrl("qrc:///main.qml"));
	quickView.show();

    return app.exec();
}

#include <QQmlApplicationEngine>

//#undef osgEarth_FOUND

int main1(int argc, char* argv[])
{
#ifdef osgEarth_FOUND
    osgEarth::initialize();
#endif

    qputenv("QSG_RENDER_LOOP", "basic");

    QGuiApplication app(argc, argv);

    QQuickWindow::setGraphicsApi(QSGRendererInterface::OpenGLRhi);

    qmlRegisterType<OsgQtQuick::OsgQuickView>("OsgQtQuick", 1, 0, "OsgQuickView");

    QSharedPointer<QQmlApplicationEngine> engine(new QQmlApplicationEngine);
    engine->load(QUrl("qrc:///main.qml"));

    {
        {
            auto* qView = engine->rootObjects()[0]->findChild<OsgQtQuick::OsgQuickView*>("osgView");
            if (!qView) return -1;

            auto* view = qView->getView();
            if (!view) return -1;

            osg::ref_ptr<osg::MatrixTransform> root(new osg::MatrixTransform);
            view->setSceneData(root);

            view->addEventHandler(new osgViewer::StatsHandler);
            view->addEventHandler(new osgViewer::HelpHandler);
            osg::ref_ptr<osgGA::CameraManipulator> mp = new osgGA::TrackballManipulator;
#ifdef osgEarth_FOUND
            mp = new osgEarth::EarthManipulator;

            QString earthFile = app.applicationDirPath() + "/../data/earth3D_gaode.earth";
            osg::Node *node = osgDB::readNodeFile(earthFile.toLocal8Bit().data());
            osgEarth::MapNode* readMapNode = osgEarth::MapNode::get(node);
            osgEarth::LayerVector out_layers;
            readMapNode->getMap()->getLayers(out_layers);

            osg::ref_ptr<osgEarth::MapNode> mapNode = new osgEarth::MapNode;
            mapNode->getMap()->beginUpdate();
            mapNode->getMap()->addLayers(out_layers);
            mapNode->getMap()->endUpdate();

            root->addChild(mapNode);// osgDB::readRefNodeFile(earthFile.toLocal8Bit().data()));
#else
            root->addChild(osgDB::readRefNodeFile("cow.osg"));
#endif

            view->setCameraManipulator(mp);
            mp->home(0);
        }
    }

    return app.exec();
}